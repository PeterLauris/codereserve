Copyright © 2014 Peter Lauris <peterlauris [at] gmail.com>
This work is free and it comes without any warranty, to the extent permitted by applicable law. You can redistribute it and/or modify it under the terms of the Do What The Fuck You Want To Public License, Version 2, as published by Sam Hocevar. See www.wtfpl.net for more details.

#CodeReserve
Šis projekts ir tapis kā praktiskais darbs kursam Tīmekļa tehnoloģijas II.
CodeReserve ir radīts cilvēkiem, kurus interesē programmēšana. Tas ļaus lietotājiem pievienot sava programmas koda fragmentus, funkcijas vai citus darbojošos piemērus tam atbilstošās kategorijās. Pārējie lapas apmeklētāji pēc tam šo kodu varēs aplūkot un vērtēt.