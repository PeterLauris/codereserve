<?php

return array(
    'NOT_LOGGED_IN' => 'Not logged in. Would you like to?',
    'LOG_OUT' => 'Log out',
    'LOGGED_IN_AS' => 'Loggged in as ',
    'CR_TITLE' => 'CodeReserve',
    'MENU_TITLE_SNIPPETS'=>'Code Snippets', 
    'MENU_TITLE_COUNTRIES'=>'Countries', 
    'NOT_SET' => 'not set');