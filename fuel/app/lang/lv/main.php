<?php

return array(
    'NOT_LOGGED_IN' => 'Tu neesi iegājis sistēmā. Ieiet?',
    'LOG_OUT' => 'Iziet',
    'LOGGED_IN_AS' => 'Ienācis kā ',
    'CR_TITLE' => 'CodeReserve',
    'MENU_TITLE_SNIPPETS' => 'Koda fragmenti',
    'MENU_TITLE_COUNTRIES' => 'Valstis',
    'NOT_SET' => 'nav noteikts',
    
);