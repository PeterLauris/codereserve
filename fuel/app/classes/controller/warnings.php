<?php

use \Model_Orm_Warnings;

class Controller_Warnings extends Controller_Public {
    private $_auth;
    private $_user_id;
    
    public function before() {
		parent::before();
		
		$this->_auth = Auth::instance();
		$userids = $this->_auth->get_user_id();
		$this->_user_id = $userids[1];

		$tmparr = Auth::instance()->get_user_id();
		if(!Auth::instance()->check())
			Response::redirect("snippets") and die();
		
		//loads messages for snippets controller
		Lang::load("snippets");
    }

    /**
     * Creation of new snippetss.
     * Works on both the first load, which is typically 
     * a GET request as on later requests, which are POST.
     * When POST-ing, a validation is run on input data.
     * Validation rules taken from "Snippet" model.
     */
    public function action_index() {
		if ( !Auth::member(100) ) {
		//if ($this->_user_id == 0){
		    Response::redirect("warnings/view/") and die();
		}
		$data = array(); //to be passed into the view
		
		if (Input::method() == "POST") {
		    $val = Model_Orm_Warnings::validate('create');
			$user = DB::select("id", "group")
			    ->from("users")
			    ->where("username", "=", Input::post("username"))
			    ->execute()
			    ->as_array();
			$tmparr = Auth::get_groups();
			if (count($user) != 1) {
				Session::set_flash("error", __('USER_NOT_FOUND'));
			}
			else if ($user[0]["group"] >= $tmparr[0][1]){
				Session::set_flash("error", __('ERR_CREATE_AUTH'));
			}
		    else if ($val->run()) {
				$newWarning = new Model_Orm_Warnings();
				$newWarning->user_id = $user[0]["id"];
				$newWarning->type = $val->validated("type");
				$newWarning->reason = $val->validated("reason");
				$newWarning->expires_at = $val->validated("expires_at");
				$newWarning->save();

				Session::set_flash("success", __('WARNING_ADDED'));
				Response::redirect("warnings/view/");
		    }
		    else {
				Session::set_flash("error", __('WARNING_SAVE_FAIL'));
		    }
		    $this->template->title = __("ACTION_CREATE_TITLE");
		    $data["form_key"] = Input::post("form_key");
		}
		else {
		    //the first GET request
		    $this->template->title = __("ACTION_CREATE_TITLE");

		    //we assign a random value to the form
		    $data["form_key"] = md5(mt_rand(1000, 10000));
		}
		
		$this->add_rich_form_scripts();
		$data["categories"] = Model_Orm_Category::get_categories();
		$this->template->page_content = View::forge("warnings/create", $data);
    }

    public function action_view() {
    	$warnings_model = array();
    	$warnings_username = "";
    	if(Auth::instance()->check() and !Auth::member(100)) {
    		$tmparr = Auth::instance()->get_user_id();
			$warnings_model = Model_Orm_Warnings::find("all", array(
				    //we only want future and current snippetss
				    "where" => array(
						array('user_id', '=', $tmparr[1])
				    ),
			    "order_by" => array("created_at" => "asc")));
		}
		else if(Auth::member(100)) {
			if (Input::method() == "POST") {
			    $val = Model_Orm_Warnings::validate('create');
				$user = DB::select("id")
				    ->from("users")
				    ->where("username", "=", Input::post("username"))
				    ->execute()
				    ->as_array();
				if (count($user) != 1) {
					Session::set_flash("error", __('USER_NOT_FOUND'));
				}
				else {
					$warnings_model = Model_Orm_Warnings::find("all", array(
					    //we only want future and current snippetss
					    "where" => array(
							array('user_id', '=', $user[0]["id"])
					    ),
				    "order_by" => array("created_at" => "asc")));

				    $warnings_username = Input::post("username");
				}
			}
		}

		//$data["snippets"] = $snippets;
		$snippets_view = View::forge("warnings/view");
		$snippets_view->set("warnings_model", $warnings_model);
		$snippets_view->set("warnings_username", $warnings_username);
		//$snippets_view->set("comments", $comments);
		$snippets_view->set("form", View::forge('comments/_form'));
		$this->template->title = __("ACTION_VIEW_TITLE");
		$this->template->page_content = $snippets_view;
		//return Response::forge('<p>return text</p>');
    }

    public function action_delete($id = null) {
    	is_null($id) and Response::redirect('warnings/view');

    	$warning = Model_Orm_Warnings::find($id);        
        if (!$warning) {
            Session::set_flash('error', __('WARNING_FIND_FAIL'));
            Response::redirect('warnings/view');
        }
        
        if (!Auth::member(100)) {
            Session::set_flash('error', __('ERR_CREATE_AUTH'));
            Response::redirect('warnings/view/');
        }

        if ($warning->delete() ) {
            Session::set_flash('success', __('SAVE_SUCCESS'));
            Response::redirect("warnings/view/");
        }
        else {
            Session::set_flash('error', __('SAVE_FAIL'));
            Response::redirect("warnings/view/");
        }
    }

    /**
     * since we have "rich form", additional scripts
     * and stylesheets are needed
     */
    private function add_rich_form_scripts() {
		$this->template->libs_js = array(
		    "http://code.jquery.com/jquery-1.8.2.js",
		    "http://code.jquery.com/ui/1.9.1/jquery-ui.js",
		    "jquery-ui-timepicker-addon.js",
		    "http://cdn.aloha-editor.org/latest/lib/require.js"
		);
		$this->template->libs_css = array(
		    "http://code.jquery.com/ui/1.9.1/themes/base/jquery-ui.css",
		    "datetimepicker.css",
		    "http://cdn.aloha-editor.org/latest/css/aloha.css"
		);
    }
}
