<?php

use \Model_Orm_Snippets;

class Controller_Snippets extends Controller_Public {
    private $_auth;
    private $_user_id;
    
    public function before() {
		parent::before();
		
		$this->_auth = Auth::instance();
		$userids = $this->_auth->get_user_id();
		$this->_user_id = $userids[1];
		
		//loads messages for snippets controller
		Lang::load("snippets");
    }

    /**
     * Demonstrates reading data through an ORM model
     */
    public function action_index() {
    	if (!Auth::instance()->check()) {
			$snippets_model = Model_Orm_Snippets::find("all", array(
				    //we only want future and current snippetss
				    "where" => array(
						array('allow_guests', '=', 1)
				    ),
			    "order_by" => array("id" => "asc"),
			    "related" => array("ratings", "users")));
		}
    	else {
			$snippets_model = Model_Orm_Snippets::find("all", array(
			    "order_by" => array("created_at" => "desc"),
			    "related" => array("ratings", "users")));
		}

		$main_content = View::forge("snippets/index");
		$main_content->set("snippets_model", $snippets_model);

		$this->template->libs_js = array(
		    "http://code.jquery.com/jquery-1.8.2.js");
		
		$this->template->page_content = $main_content;
    }
    
    /**
     * Creation of new snippetss.
     * Works on both the first load, which is typically 
     * a GET request as on later requests, which are POST.
     * When POST-ing, a validation is run on input data.
     * Validation rules taken from "Snippet" model.
     */
    public function action_create() {
		if ( ! Auth::has_access('snippet.create') ) {
		//if ($this->_user_id == 0){
		    Session::set_flash("error", __('ERR_CREATE_AUTH'));
		    Response::redirect("/") and die();
		}
		$data = array(); //to be passed into the view

		if (Input::method() == "POST") {
		    $val = Model_Orm_Snippets::validate('create');
		    if ($val->run()) {
				$newSnippet = new Model_Orm_Snippets();
				$tmparr = Auth::instance()->get_user_id();
				$newSnippet->user_id = $tmparr[1];
				$newSnippet->category_id = $val->validated("category");
				$newSnippet->title = $val->validated("title");
				$newSnippet->description = $val->validated("description");
				$newSnippet->content = $val->validated("snippet");
				$newSnippet->allow_guests = $val->validated("allow_guests");
				//first, we save the item without attachments
				$newSnippet->save();

				//$errors = $this->try_get_attachments($newSnippet);

				Session::set_flash("success", __('ACTION_CREATE_CREATED'));
				Response::redirect("snippets/view/" . $newSnippet->id);
		    }
		    else {
				Session::set_flash("error", __('ACTION_SAVE_FAIL'));
		    }
		    $this->template->title = __("ACTION_CREATE_TITLE");
		    $data["form_key"] = Input::post("form_key");
		}
		else {
		    //the first GET request
		    $this->template->title = __("ACTION_CREATE_TITLE");

		    //we assign a random value to the form
		    $data["form_key"] = md5(mt_rand(1000, 10000));
		}
		
		$this->add_rich_form_scripts();
		$data["categories"] = Model_Orm_Category::get_categories();
		$this->template->page_content = View::forge("snippets/create", $data);
    }

    public function action_edit($id = null) {
		//looks up the even in the database
		//if anything is not OK - redirects back to the list of snippetss

		is_null($id) and Response::redirect('snippets');
		$snippet = Model_Orm_Snippets::find($id, array("related" =>
			    array("ratings", "users")));

		is_null($snippet) and Response::redirect('snippets');

		$tmparr = Auth::instance()->get_user_id();
		if (!Auth::member(100) && $tmparr[1] != $snippet->user_id) {
            Session::set_flash('error', __('ERR_CREATE_AUTH'));
            Response::redirect('snippets/view/' . $snippet->id);
        }

		//not POST = just read from database
		if (Input::method() == 'POST') {
		    $val = Model_Orm_Snippets::validate("edit");
		    if ($val->run()) {
				//validation is OK!

				$snippet->category_id = $val->validated("category");
				$snippet->title = $val->validated("title");
				$snippet->description = $val->validated("description");
				$snippet->content = $val->validated("snippet");
				$snippet->allow_guests = $val->validated("allow_guests");

				//$snippet->location = Model_Orm_Location::find(Input::post("location"));
				if ($snippet->save()) {
				    Session::set_flash("success", __('SAVE_SUCCESS'));
				} else {
				    Session::set_flash("error", __('SAVE_FAIL'));
				}

				//Response::redirect("snippets/view/" . $snippet->id);
			    } else {
				//POST data passed, but something wrong with validation
				Session::set_flash("error", $val->error());
		    }
		}

		$data["snippet"] = $snippet;
		$this->add_rich_form_scripts();
		$data["categories"] = Model_Orm_Category::get_categories();
		$this->template->title = __('SNIPPET_EDIT'); //$snippet->title;
		$this->template->page_content = View::forge("snippets/edit", $data);
    }

    /**
     * Displays information about the snippets
     * @param int $id Database ID of the item
     */
    public function action_view($id = null) {
	
		is_null($id) and Response::redirect('snippets');

		$user_rating = 0;
		$tmparr = Auth::instance()->get_user_id();
		$exist_rating = DB::select("rating")
		    ->from("ratings")
		    ->where("user_id", "=", $tmparr[1])
		    ->where("snippet_id", "=", $id)
		    ->execute()
		    ->as_array();
		if($exist_rating)
			$user_rating = $exist_rating[0]["rating"];
		//////////////////
		if(Input::post()) {
			if (Auth::instance()->check()) {

				if(Input::post("comment")) {
					$comment = Model_Orm_Comment::forge(array(
			            'user_id' => $tmparr[1],
			            'comment' => Input::post('comment'),
			            'snippet_id' => Input::post('snippet_id'),
			        ));

			        if($comment and $comment->save()) {
			        	Session::set_flash('success', __('ACTION_CREATE_COMMENT'));
		            	Response::redirect('snippets/view/'.$comment->snippet_id);
			        }
			        else {
			            Session::set_flash('error', __('SAVE_FAIL'));
			        }
			    }
			    else if(Input::post("rating")) {
			    	if(((int)Input::post("rating") >= 1 and (int)Input::post("rating") <= 5) and (int)Input::post("snippet_id") > 0){
						
			    		if($user_rating == 0) {
					    	$rating = Model_Orm_Rating::forge(array(
					            'user_id' => $tmparr[1],
					            'snippet_id' => Input::post('snippet_id'),
					            'rating' => Input::post('rating'),
					        ));

					        if($rating and $rating->save()) {
					        	$user_rating = Input::post('rating');
					        	Session::set_flash('success', __('RATING_ADDED'));
				            	Response::redirect('snippets/view/'.$rating->snippet_id);
					        }
					        else {
					            Session::set_flash('error', __('SAVE_FAIL'));
					        }
					    }
					    else {
					    	$exist_rating = DB::update("ratings")
							    ->where("user_id", "=", $tmparr[1])
							    ->where("snippet_id", "=", $id)
							    ->value('rating', (int)Input::post("rating"))
							    ->execute();
					    }

					    $user_rating = (int)Input::post("rating");
			    	}
			    	else {
			    		Session::set_flash('error', __('INVALID_DATA'));
			    	}
			    }
			}
			else {
				Session::set_flash('error', __('ERR_CREATE_AUTH'));
			}
		}
		else {
			$this->template->set_global('message', $id, false);
		}

		$tmparr = Model_Orm_Rating::get_average_rating($id);
		$average_rating = $tmparr[0];

		//$this->template->title = 'Comments &raquo; Create';
		$data['form'] = View::forge('comments/_form');
		//$this->template->content = View::forge('comments/create', $data);
		//////////////////
		
		$snippets = Model_Orm_Snippets::find($id, array("related" =>
			    array("ratings", "users", "category")));

		if(is_null($snippets)) {
			Session::set_flash('error', __('SNIPPET_NOT_FOUND'));
			Response::redirect('snippets');
		}

		if (!Auth::instance()->check()) {
			if (!$snippets->allow_guests) {
				Session::set_flash('error', __('ERR_CREATE_AUTH'));
				Response::redirect('snippets');
			}
		}

		/*$comments = Model_Orm_Comment::find('all', array('where' => array('snippet_id' => $id)));*/
 
		$snippets_view = View::forge("snippets/view");
		$snippets_view->set("snippets", $snippets);
		$snippets_view->set("average_rating", $average_rating);
		$snippets_view->set("user_rating", $user_rating);
		//$snippets_view->set("comments", $comments);
		$snippets_view->set("form", View::forge('comments/_form'));
		$this->template->title = __("ACTION_VIEW_TITLE");
		$this->template->page_content = $snippets_view;
		//return Response::forge('<p>return text</p>');
    }

	public function action_delete($snippet_id = null) {
        
        is_null($snippet_id) and Response::redirect("snippets");
        
        $snippet = Model_Orm_Snippets::find($snippet_id);        
        if (!$snippet) {
            Session::set_flash('error', __('SNIPPET_NOT_FOUND'));
            Response::redirect('snippets');
        }
        $tmparr = Auth::instance()->get_user_id();
        if (!Auth::member(100) && $tmparr[1] != $snippet->user_id) {
            Session::set_flash('error', __('ERR_CREATE_AUTH'));
            Response::redirect('snippets/view/' . $snippet->id);
        }

        if ($snippet->delete() ) {
            Session::set_flash('success', __('SNIPPET_DELETE_SUCCESS'));
            Response::redirect("snippets");
        }
        else {
            Session::set_flash('error', __('SNIPPET_DELETE_FAIL'));
            Response::redirect("snippets/view/" . $snippet_id);
        }
        
    }

    /**
     * since we have "rich form", additional scripts
     * and stylesheets are needed
     */
    private function add_rich_form_scripts() {
		$this->template->libs_js = array(
		    "http://code.jquery.com/jquery-1.8.2.js",
		    "http://code.jquery.com/ui/1.9.1/jquery-ui.js",
		    "jquery-ui-timepicker-addon.js",
		    "http://cdn.aloha-editor.org/latest/lib/require.js"
		);
		$this->template->libs_css = array(
		    "http://code.jquery.com/ui/1.9.1/themes/base/jquery-ui.css",
		    "datetimepicker.css",
		    "http://cdn.aloha-editor.org/latest/css/aloha.css"
		);
    }
}
