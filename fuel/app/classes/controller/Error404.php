<?php

use \Model_Orm_Error404;

class Controller_Error404 extends Controller_Public {
    private $_auth;
    private $_user_id;
    
    public function before() {
		parent::before();
		
		$this->_auth = Auth::instance();
		$userids = $this->_auth->get_user_id();
		$this->_user_id = $userids[1];
		
		//loads messages for snippets controller
		Lang::load("snippets");
    }

    /**
     * Demonstrates reading data through an ORM model
     */
    public function action_index() {

		$main_content = View::forge("Error404/index");

		$this->template->libs_js = array(
		    "http://code.jquery.com/jquery-1.8.2.js");
		
		$this->template->page_content = $main_content;
    }
}
