<?php

class Controller_Comments extends Controller_Public
{

	public function action_edit($id = null)
	{
		$comment = Model_Orm_Comment::find($id);
		if (!Auth::member(100) && Auth::instance()->get_user_id()[1] != $comment->user_id) {
            Session::set_flash('error', __('ERR_CREATE_AUTH'));
            Response::redirect('snippets/view/' . $comment->snippet_id);
        }
		if (Input::post())
	    {
			$val = Model_Orm_Comment::validate("edit");
            if ($val->run()) {
                $comment->comment = $val->validated("comment");
                if ($comment->save()) {
		            Session::set_flash('success', __('COMMENT_UPDATE'));
		        }
		        else {
		            Session::set_flash('error', __('COMMENT_UPDATE_FAIL'));
		        }

                Response::redirect("snippets/view/" . $comment->snippet_id);
            }
            else {
                //POST data passed, but something wrong with validation
                Session::set_flash("error", $val->error());
                Response::redirect("snippets/view/" . $comment->snippet_id);
            }
	    }
        $this->template->set_global('comment', $comment, false);
        $this->template->set_global('message', $comment->snippet_id, false);
		$this->template->title = __('COMMENT_EDIT_TITLE');
		$data['form'] = View::forge('comments/_form');
		$this->template->content = View::forge('comments/edit', $data);
	}

	public function action_create($id = null)
	{
		if(Input::post()) {

			$comment = Model_Orm_Comment::forge(array(
	            'user_id' => Auth::instance()->get_user_id()[1],
	            'comment' => Input::post('comment'),
	            'snippet_id' => Input::post('snippet_id'),
	        ));

			if (!Auth::instance()->check()) {
		        Session::set_flash('error', __('ERR_CREATE_AUTH'));
		        Response::redirect('snippets/view/' . $comment->snippet_id);
		    }

	        if($comment and $comment->save()) {
	        	Session::set_flash('success', __('COMMENT_ADD'));
            	Response::redirect('snippets/view/'.$comment->snippet_id);
	        }
	        else {
	            Session::set_flash('error', __('COMMENT_ADD_FAIL'));
	        }
		}
		else {
			$this->template->set_global('message', $id, false);
		}

		$this->template->title = __('COMMENT_CREATE_TITLE');
		$data['form'] = View::forge('comments/_form');
		$this->template->content = View::forge('comments/create', $data);
	}

	public function action_delete($comment_id = null) {
        
        is_null($comment_id) and Response::redirect("snippets");
        
        $comment = Model_Orm_Comment::find($comment_id);        
        if (!$comment) {
            Session::set_flash('error', __('COMMENT_NOT_FOUND'));
            Response::redirect('snippets');
        }
        
        if (!Auth::member(100) && Auth::instance()->get_user_id()[1] != $comment->user_id) {
            Session::set_flash('error', __('ERR_CREATE_AUTH'));
            Response::redirect('snippets/view/' . $comment->snippet_id);
        }
        
        $snippet_id = $comment->snippet_id;

        if ($comment->delete() ) {
            Session::set_flash('success', __('COMMENT_DELETE_SUCCESS'));
            Response::redirect("snippets/view/" . $snippet_id);
        }
        else {
            Session::set_flash('error', __('COMMENT_DELETE_FAIL'));
            Response::redirect("snippets/view/" . $snippet_id);
        }
        
    }

}
