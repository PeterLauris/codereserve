<?php

use \Model_Orm_Snippets;

class Controller_Search extends Controller_Public {
    private $_auth;
    private $_user_id;
    
    public function before() {
		parent::before();
		
		$this->_auth = Auth::instance();
		$userids = $this->_auth->get_user_id();
		$this->_user_id = $userids[1];
		
		//loads messages for snippets controller
		Lang::load("snippets");
    }

    /**
     * Demonstrates reading data through an ORM model
     */
    public function action_index() {
		$main_content = View::forge("search/index");
		$search_results = array();
    	if(Input::post()) {
    		$query = DB::select("*")->from("snippets");

    		if(Input::post("category")>=0)
				$query->where("category_id", "=", Input::post("category"));
			if(Input::post("keyword")) {
				$query->where(function($query){
					$query->where('title', 'LIKE', '%'.Input::post("keyword").'%')
						  ->or_where('description', 'LIKE', '%'.Input::post("keyword").'%');
				});
			}

	    	if (!Auth::instance()->check())
				$query->where("allow_guests", "=", 1);
			
			$search_results = $query->execute()->as_array();

		    $main_content->set("form_key", Input::post("form_key"));
		}
		else {
		    $main_content->set("form_key", md5(mt_rand(1000, 10000)));
		}

		$this->template->libs_js = array(
		    "http://code.jquery.com/jquery-1.8.2.js");

		$main_content->set("search_keyword", Input::post("keyword"));
		$main_content->set("search_category", Input::post("category"));
		$main_content->set("search_results", $search_results);
		$main_content->set("categories", Model_Orm_Category::get_categories());
		$this->template->page_content = $main_content;
    }

    /**
     * since we have "rich form", additional scripts
     * and stylesheets are needed
     */
    private function add_rich_form_scripts() {
		$this->template->libs_js = array(
		    "http://code.jquery.com/jquery-1.8.2.js",
		    "http://code.jquery.com/ui/1.9.1/jquery-ui.js",
		    "jquery-ui-timepicker-addon.js",
		    "http://cdn.aloha-editor.org/latest/lib/require.js"
		);
		$this->template->libs_css = array(
		    "http://code.jquery.com/ui/1.9.1/themes/base/jquery-ui.css",
		    "datetimepicker.css",
		    "http://cdn.aloha-editor.org/latest/css/aloha.css"
		);
    }
}
