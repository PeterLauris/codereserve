<?php

class Model_Orm_Comment extends \Orm\Model
{
	protected static $_table_name = 'comments';
	protected static $_primary_key = array('id');
	protected static $_properties = array(
			'id',
			'user_id',
			'comment',
			'snippet_id',
			'created_at',
			'updated_at'
		);
   
     protected static $_belongs_to = array(
			'snippet' => array(
				'key_from' => 'snippet_id',
				'model_to' => 'Model_Orm_Snippets',
				'key_to' => 'id',
		        'cascade_save' => true,
		        'cascade_delete' => false),
            'user' => array(
				'key_from' => 'user_id',
				'model_to' => 'Model_Orm_User',
				'key_to' => 'id')
		);
    
    public static function validate($factory) {
		$val = Validation::forge($factory);
		$val->add_field('comment', 'comment', 'required');
		return $val;
    }

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'comments' => array('before_insert'),
			'mysql_timestamp' => true,
		),
		'Orm\Observer_UpdatedAt' => array(
			'comments' => array('before_save'),
			'mysql_timestamp' => true,
		),
	);
}
