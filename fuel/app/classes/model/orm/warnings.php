<?php

class Model_Orm_Warnings extends Orm\Model {
    
    protected static $_table_name = 'warnings';
    protected static $_primary_key = array('id');
    protected static $_properties = array(
			'id',
			'user_id',
			'type',
			'reason',
			'created_at',
			'expires_at'
		);
    
    protected static $_belongs_to = array(
		'users' => array(
		    'key_from' => 'user_id',
		    'model_to' => 'Model_Orm_User',
		    'key_to' => 'id',
	        'cascade_save' => true,
	        'cascade_delete' => false)
    );
    
    /*protected static $_properties = array(
		'id',
		'title' => array(
		    'validation'=>array('required'),
		    'data_type' => 'varchar',
		    'label' => 'Title of the event'),
		'description' => array(
		    'data_type' => 'text',
		    'label' => 'Description: what will happen?'),
		'start' => array(
		    'data_type' => 'date',
		    'label' => 'Start date and time of the event'
		),
		'location_id' => array( 
			"form"=>array("type"=>true)
		),
		'poster' => array(
		    'data_type' => 'varchar',
		    'label' => 'Poster of the event (PDF document)'),
    );*/

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'comments' => array('before_insert'),
			'mysql_timestamp' => true,
		)
	);

    public static function validate($factory) {
		$val = Validation::forge($factory);

		//because we want to check if location is valid
		//$val->add_callable("Model_Orm_Location");

		$val->add_field('type', 'Type', 'required');
		$val->add_field('reason', 'Reason', 'required');
		$val->add_field('expires_at', 'Expires at', 'required');
		return $val;
    }

}
