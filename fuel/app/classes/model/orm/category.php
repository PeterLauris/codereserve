<?php

class Model_Orm_Category extends Orm\Model {
    
    protected static $_table_name = 'categories';
	protected static $_primary_key = array('id');
	protected static $_properties = array(
			'id',
			'title'
		);

    /*protected static $_belongs_to = array(
		'snippets' => array(
		    'key_from' => 'id',
		    'model_to' => 'Model_Orm_Snippets',
		    'key_to' => 'category_id')
    );*/
   
    public static function get_categories() {
		return \DB::select('id', 'title')
					    ->from('categories')
					    ->execute()
					    ->as_array('id', 'title');
    }
    
    /*public static function _validation_valid_category($val)
    {
		if (Model_Orm_Category::find($val)==null) {
		    Validation::active()->set_message('valid_category', 
				'The field "category" must be a valid option.');
		    return false;
		}
		return true;
    }*/

    /*public static function validate($factory) {
		$val = Validation::forge($factory);

		//because we want to check if location is valid
		$val->add_callable("Model_Orm_Location");

		$val->add_field('title', 'Title', 'required|max_length[63]');
		$val->add_field('description', 'Description', 'max_length[255]');
		$val->add_field('snippet', 'Content', 'required|min_length[1]');
		//$val->add_field('location', 'Event location', 'required|valid_location');
		return $val;
    }*/

}
