<?php

class Model_Orm_Snippets extends Orm\Model {
    
    protected static $_table_name = 'snippets';
    protected static $_primary_key = array('id');
    protected static $_properties = array(
			'id',
			'user_id',
			'category_id',
			'title',
			'description',
			'description',
			'content',
			'allow_guests',
			'created_at',
			'updated_at'
		);
    
    protected static $_has_many = array(
        'ratings' => array(
            'key_from' => 'id',
            'model_to' => 'Model_Orm_Rating',
            'key_to' => 'snippet_id',
            'cascade_save' => true,
            'cascade_delete' => true),
        'comments' => array(
            'key_from' => 'id',
            'model_to' => 'Model_Orm_Comment',
            'key_to' => 'snippet_id',
            'cascade_save' => true,
            'cascade_delete' => true)
    );

    protected static $_belongs_to = array(
        'category' => array(
            'key_from' => 'category_id',
            'model_to' => 'Model_Orm_Category',
            'key_to' => 'id',
            'cascade_save' => false,
            'cascade_delete' => false),
		'users' => array(
		    'key_from' => 'user_id',
		    'model_to' => 'Model_Orm_User',
		    'key_to' => 'id')
    );

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'snippets' => array('before_insert'),
			'mysql_timestamp' => true,
		),
		'Orm\Observer_UpdatedAt' => array(
			'snippets' => array('before_save'),
			'mysql_timestamp' => true,
		),
	);
    
    /*protected static $_properties = array(
		'id',
		'title' => array(
		    'validation'=>array('required'),
		    'data_type' => 'varchar',
		    'label' => 'Title of the event'),
		'description' => array(
		    'data_type' => 'text',
		    'label' => 'Description: what will happen?'),
		'start' => array(
		    'data_type' => 'date',
		    'label' => 'Start date and time of the event'
		),
		'location_id' => array( 
			"form"=>array("type"=>true)
		),
		'poster' => array(
		    'data_type' => 'varchar',
		    'label' => 'Poster of the event (PDF document)'),
    );*/

    public static function validate($factory) {
		$val = Validation::forge($factory);

		//because we want to check if location is valid
		//$val->add_callable("Model_Orm_Category");

		$val->add_field('category', 'Category', 'required');
		$val->add_field('title', 'Title', 'required|max_length[63]');
		$val->add_field('description', 'Description', 'max_length[255]');
		$val->add_field('snippet', 'Content', 'required|min_length[1]');
		$val->add_field('allow_guests', 'Allow guests', 'required');
		//$val->add_field('category', 'Event location', 'required|valid_category');
		return $val;
    }

}
