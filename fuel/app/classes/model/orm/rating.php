<?php

class Model_Orm_Rating extends Orm\Model {
    
    protected static $_table_name = 'ratings';
    protected static $_primary_key = array('id');
    protected static $_properties = array(
			'id',
			'user_id',
			'snippet_id',
			'rating',
			'created_at',
			'updated_at'
		);
    
    protected static $_belongs_to = array(
		'users' => array(
		    'key_from' => 'id',
		    'model_to' => 'Model_Orm_User',
		    'key_to' => 'id'),
		'snippets' => array(
		    'key_from' => 'id',
		    'model_to' => 'Model_Orm_Snippets',
		    'key_to' => 'id',
	        'cascade_save' => true,
	        'cascade_delete' => false)
    );
   
    public static function get_average_rating($id) {
		$expression_avg = \DB::expr('AVG(rating)');
		$expression_count = \DB::expr('COUNT(rating)');
		return \DB::select(array($expression_avg,'average'), array($expression_count,'ratings_count'))->from('ratings')->where('snippet_id', '=', $id)->execute();
    }
    
    /*protected static $_properties = array(
		'id',
		'title' => array(
		    'validation'=>array('required'),
		    'data_type' => 'varchar',
		    'label' => 'Title of the event'),
		'description' => array(
		    'data_type' => 'text',
		    'label' => 'Description: what will happen?'),
		'start' => array(
		    'data_type' => 'date',
		    'label' => 'Start date and time of the event'
		),
		'location_id' => array( 
			"form"=>array("type"=>true)
		),
		'poster' => array(
		    'data_type' => 'varchar',
		    'label' => 'Poster of the event (PDF document)'),
    );*/

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'comments' => array('before_insert'),
			'mysql_timestamp' => true,
		),
		'Orm\Observer_UpdatedAt' => array(
			'comments' => array('before_save'),
			'mysql_timestamp' => true,
		),
	);

    /*public static function validate($factory) {
		$val = Validation::forge($factory);

		//because we want to check if location is valid
		//$val->add_callable("Model_Orm_Location");

		$val->add_field('title', 'Title', 'required|max_length[255]');
		$val->add_field('start', 'Start date', 'required|max_length[50]');
		$val->add_field('location', 'Event location', 'required|valid_location');
		$val->add_field('description', 'Description', 'max_length[2000]');
		return $val;
    }*/

}
