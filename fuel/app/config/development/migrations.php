<?php
return array(
	'version' => 
	array(
		'app' => 
		array(
			'default' => 
			array(
				0 => '001_create_structure',
				1 => '002_demo_data',
				2 => '003_create_comments',
				3 => '004_create_snippets',
			),
		),
		'module' => 
		array(
		),
		'package' => 
		array(
		),
	),
	'folder' => 'migrations/',
	'table' => 'migration',
);
