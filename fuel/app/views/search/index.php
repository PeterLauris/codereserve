<h2><?php echo __("ACTION_SEARCH_TITLE")?></h2>
<?php 
   echo Form::open(
	    array("enctype" => "multipart/form-data")
	   ); ?>
<fieldset>
    <div class="clearfix">
	<?php echo Form::label(__('KEYWORD'), 'keyword'); ?>
	<div class="input">
	    <?php
	    	echo Form::input('keyword', isset($search_keyword) ? $search_keyword : '', array("class" => "span4"));
	    ?>
	</div>
    </div>

	<div class="clearfix">
	<?php echo Form::label(__('ACTION_VIEW_LABEL_CATEGORY'), 'category'); ?>
	<div class="input">
	    <?php
	    $category_options = array_merge($categories);
	    echo Form::select("category", Input::post("category", isset($search_category) ? $search_category : 0), $category_options);
	    ?>
	</div>
    </div>
</fieldset>	
<div class="actions">
    <?php echo Form::hidden("form_key", $form_key); ?>
    <?php echo Form::submit('submit', __('SEARCH'), array('class' => 'btn')); ?>

</div>
<?php echo Form::close();

foreach ($search_results as $result) :
    ?>
    <h3><?php
    echo Html::anchor("snippets/view/" . $result["id"], $result["title"]);
    ?></h3>
    
    <div class="description" data-result-id="<?php echo $result["id"]?>">
	   <?php echo $result["description"]; ?>
    </div>
    <hr>
    
<?php endforeach; ?>