<?php echo Form::open(); ?>
<p>
	<?php echo Form::label(__('COMMENT_ADD_TITLE'), 'comment'); ?>
	<?php echo Form::textarea('comment', Input::post('comment', isset($comment) ? $comment->comment : ''), array('cols' => 60, 'rows' => 5)); ?>
</p>
<?php echo Form::hidden('snippet_id', Input::post('snippet_id', isset($message) ? $message : '')); ?>
<div class="actions">
    <?php echo Form::submit(); ?>
</div>
<?php echo Form::close(); ?>