<?php
echo Form::open();
echo Form::fieldset_open(null, "Enter your data");
?>

<label for="username">Username</label>
<input type="text" name="username" value="<?php echo Input::post('username'); ?>" id="username" />

<label for="usermail">E-mail</label>
<input type="text" name="usermail" value="<?php echo Input::post('usermail'); ?>" id="usermail" />

<label for="usermail_rep">E-mail (again)</label>
<input type="text" name="usermail_rep" id="usermail_rep" />

<label for="password">Password</label>
<input type="password" name="password" id="password" />

<label for="password_rep">Password (again)</label>
<input type="password" name="password_rep" id="password_rep" />
<br />
<input type="Submit" value="Register" class="btn" />
<?php
echo Form::fieldset_close();
echo Form::close();

