<?php
echo Form::open();
echo Form::fieldset_open(null, __('LOG_IN_TITLE'));
?>

<label for="username"><?php echo __('USERNAME_OR_EMAIL'); ?></label>
    <input type="text" name="username" id="username" />
<label for="password"><?php echo __('PASSWORD'); ?></label>
    <input type="password" name="password" id="password" />
<br />
<input type="Submit" value="<?php echo __('LOG_IN'); ?>" />
<?php
echo Form::fieldset_close();
echo Form::close();
?>

<div id="register">
    <?php
    echo Html::anchor("account/create", __('NOT_REGISTERED'));
    ?>
</div>
