<h2><?php echo __("ACTION_CREATE_DESCRIBE")?></h2>

<?php 
echo Form::open(
    array("enctype" => "multipart/form-data")
   ); ?>
<fieldset>
    <div class="clearfix">
	<?php echo Form::label(__('ACTION_CREATE_LABEL_TITLE'), 'title'); ?>
	<div class="input">
	    <?php
	    	echo Form::input('title', Input::post('title'), array("class" => "span4"));
	    ?>
	</div>
    </div>

    <div class="clearfix">
	<?php echo Form::label(__('ACTION_CREATE_LABEL_DESCRIPTION'), 'description'); ?>
	<div class="input">
	    <?php
	    echo Form::textarea('description', Input::post('description'), array("id" => "description", "rows" => 2, "class" => "span4"));
	    ?>
	</div>
    </div>

    <div class="clearfix">
	<?php echo Form::label(__('ACTION_CREATE_LABEL_SNIPPET'), 'snippet'); ?>
	<div class="input">
	    <?php
	    echo Form::textarea('snippet', Input::post('snippet'), array("id" => "snipet", "rows" => 5, "class" => "span4"));
	    ?>
	</div>
	</div>

	<div class="clearfix">
	<?php echo Form::label(__('ACTION_VIEW_LABEL_CATEGORY'), 'category'); ?>
	<div class="input">
	    <?php
	    $category_options = array_merge($categories);
	    echo Form::select("category", Input::post("category"), $category_options);
	    ?>
	</div>
    </div>

    <div class="clearfix">
	<?php echo Form::label(__('ACTION_CREATE_LABEL_GUESTS'), 'snippet'); ?>
	<div class="input">
	    <?php
	    echo "<table>";
	    echo "<tr><td>".Form::radio('allow_guests', '1', (Input::post('allow_guests')=='' or (Input::post('allow_guests') == 1))?true:null)."</td>";
		echo "<td>".Form::label(__('ALLOW'), 'allow_guests')."</td></tr>";
		echo "<tr><td>".Form::radio('allow_guests', '0', (Input::post('allow_guests') == '0')?true:null)."</td>";
		echo "<td>".Form::label(__('DENY'), 'allow_guests')."</td></tr>";
		echo "</table>";
	    ?>
	</div>
	</div>

	</fieldset>	
<div class="actions">
    <?php echo Form::hidden("form_key", $form_key); ?>
    <?php echo Form::submit('submit', __('SAVE'), array('class' => 'btn btn-primary')); ?>

</div>
<?php echo Form::close() ?>
