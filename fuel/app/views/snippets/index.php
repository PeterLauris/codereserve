<?php
foreach ($snippets_model as $snippet) :
    ?>
    <h3><?php
    echo Html::anchor("snippets/view/" . $snippet->id, $snippet->title);
    ?></h3>
    
    <div class="description" data-snippet-id="<?php echo $snippet->id?>">
	   <?php echo $snippet->description; ?>
    </div>
    <hr>
    
<?php endforeach; ?>
<?php if (Auth::has_access("snippet.create")) : ?>
    <p>
	<?php
	echo Html::anchor("/snippets/create/", __("ADD_SNIPPET_LINK"), array("class" => "btn"))
	?>

    </p>
 <?php endif;
 