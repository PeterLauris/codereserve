<?php
$tmparr = Auth::instance()->get_user_id();
if (Auth::member(100) || $tmparr[1] == $snippets->user_id): ?>
    <p><?php echo Html::anchor('snippets/edit/'.$snippets->id, __('OPT_EDIT')); ?> |
    <?php echo Html::anchor('snippets/delete/'.$snippets->id, __('OPT_DELETE'), array('onclick' => "return confirm('". __('ARE_YOU_SURE') ."')")); ?></p>
<?php endif;
echo $snippets->updated_at; ?>
<p><strong><?php echo __("ACTION_VIEW_LABEL_CATEGORY")?>:</strong>
    <?php echo $snippets->category->title; ?></p>
<p>
    <h2><?php echo $snippets->title ?></h2></p>


<p><?php echo  $snippets->description; ?></p>

<p><?php echo  "<pre>".$snippets->content."</pre>"; ?></p>

<?php
if ($average_rating["ratings_count"] > 0)
    echo "<p>". __('SNIPPET_AVERAGE_RATING') . round($average_rating["average"], 2) . " (" . $average_rating["ratings_count"] . " " . Inflector::pluralize('rating', (int)$average_rating["ratings_count"]). ")</p>";
else
    echo "<p>". __('SNIPPET_AVERAGE_RATING_NONE') ."</p>";
if(Auth::instance()->check()):
    if($user_rating > 0){
        echo "<p>" . __('SNIPPET_YOUR_RATING') . $user_rating . "</p>";
    }
    else {
        echo __('SNIPPET_YOUR_RATING_NONE');
    }

    echo Form::open(
        array("enctype" => "multipart/form-data")
       ); ?>
    <p>
        <?php echo Form::label(__('RATING'), 'rating'); ?>
        <?php echo Form::select('rating', Input::post('rating', (isset($user_rating) and $user_rating > 0) ? $user_rating : ''), array(5 => '5', 4 => '4', 3 => '3', 2 => '2', 1 => '1')); ?>
    </p>

    <?php echo Form::hidden('snippet_id', Input::post('snippet_id', isset($snippets) ? $snippets->id : '')); ?>
    <div class="actions">
        <?php echo Form::submit(); ?>
    </div>
    <?php echo Form::close();
else:
    echo __('SNIPPET_LOG_IN');
endif;
?>
<hr>
<div id="comments">
<?php if (count($snippets->comments)) : ?>
    <h3><?php echo __('COMMENTS'); ?></h3>
    <?php
    //print_r ($snippet);
     foreach ($snippets->comments as $comment) : ?>
        <div class="comment">
            <span class="comment-username"><?php
            echo $comment->user->username;
            ?></span>
            <span class="comment-date"><?php
            echo $comment->updated_at;
            ?></span>
            <div><?php echo $comment->comment; ?></div>
            <?php if (Auth::member(100) || $tmparr[1] == $comment->user_id): ?>
                <div><?php echo Html::anchor('comments/edit/'.$comment->id, 'Edit'); ?> |
                <?php echo Html::anchor('comments/delete/'.$comment->id, 'Delete', array('onclick' => "return confirm('". __('ARE_YOU_SURE') ."')")); ?></div>
            <?php endif; ?>
        </div>
    <?php endforeach; ?>
<?php else : ?>
     <p><?php echo __('NO_COMMENTS'); ?></p>
<?php endif; ?>
<?php if (Auth::instance()->check()): ?>
    <?php $message = isset($message) ? $message : ''; ?>
        <h4 class="first"><?php  ?></h4>
    <?php echo $form; ?>
<?php else : ?>
    <p><?php echo __('COMMENT_LOG_IN'); ?></p>
<?php endif; ?>
</div>

<p>
    <?php echo Html::anchor('snippets', __("ACTION_VIEW_BACK"), array("class" => "btn-link")); ?>
</p>