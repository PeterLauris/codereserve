<?php
if (Auth::member(100)):
echo Form::open(
    array("enctype" => "multipart/form-data")
   ); ?>
<p>
    <?php echo Form::label(__('USERNAME'), 'username');
    echo Form::input('username', '', array("class" => "span4")); ?>
</p>

<div class="actions">
    <?php echo Form::submit(); ?>
</div>
<?php echo Form::close();
endif; ?>

<h3><?php echo $warnings_username; ?></h3>
<?php foreach ($warnings_model as $warning) :
    ?>
    <h5><?php
    echo ($warning->type==1) ? "Warning" : "Ban";
    ?></h5>
    
    <p><?php echo __('REASON') . ": " . $warning->reason; ?></p>
    <p><?php echo __('START_DATE') . ": " . $warning->created_at; ?></p>
    <p><?php echo __('END_DATE') . ": " . $warning->expires_at; ?></p>
    <?php if(Auth::member(100)) echo Html::anchor("warnings/delete/" . $warning->id, "Delete warning"); ?>
    <hr>
    
<?php endforeach; ?>
<?php if ( Auth::member(100) ): ?>
    <h4><?php echo Html::anchor('/warnings', __('CREATE_WARNINGS')); ?></h4>
<?php endif; ?>