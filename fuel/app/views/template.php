<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?php
if (isset($page_title)) {
    echo $page_title;
}
?></title>
	<?php
	if (isset($libs_js)) {
	    //some views may want to add extra scripts
	    echo Asset::js($libs_js);
	}
	?>

	<?php echo Asset::css('bootstrap.css'); ?>
	<?php echo Asset::css('codereserve.css'); ?>
	<?php
	if (isset($libs_css)) {
	    //some views may want to add extra stylesheets
	    echo Asset::css($libs_css);
	}
	?>
    </head>
    <body>
	<header>
		<div>
		    <h1 id="pagetitle"><?php echo Html::anchor("/", __("CR_TITLE"));?></h1>
		    
		</div>
		<div id="right-cont">
		    <div id="setlang">
			<?php
			    echo Html::anchor("account/setlang/lv", "LV");
			    echo " | ";
			    echo Html::anchor("account/setlang/en", "EN");
			?>
		    </div>
		    <div id="auth">	    
				<?php
				$auth = Auth::instance();
				$user_id = $auth->get_user_id();
				if ($user_id[1] != 0) :
				    ?>
					<span id="logged-in">
					    <?php echo __("LOGGED_IN_AS") . $auth->get_email(); ?>
					</span>
					<span id="logout">
						<?php
						echo Html::anchor("account/logout", __("LOG_OUT"));
						?>
			    	</span>
				    <?php
				else :
				    echo Html::anchor("account/simpleauth", __("NOT_LOGGED_IN"));
				    ?>
				<?php
				endif;
				?>
		    </div>
	    </div>
	    <h3><?php
	if (isset($page_title)) {
	    echo $page_title;
	}
	?></h3>
	    <nav>
		<ul class="clearfix">
		    <li><?php echo Html::anchor('/snippets', __('MENU_TITLE_SNIPPETS')); ?></li>
		    <li><?php echo Html::anchor('/search', __('SEARCH')); ?></li>
			<li><?php if (Auth::instance()->check()) echo Html::anchor('/warnings', __('WARNINGS')); ?></li>
		</ul>
	    </nav>
	</header>
	<section id="main">

	    <div class="row">
		<?php if (Session::get_flash('success')): ?>
    		<div class="alert-message success">
    		    <p>
			    <?php echo implode('</p><p>', e((array) Session::get_flash('success'))); ?>
    		    </p>
    		</div>
		<?php endif;
		if (Session::get_flash('error')): ?>
    		<div class="alert-message error">
    		    <p>
			    <?php echo implode('</p><p>', e((array) Session::get_flash('error'))); ?>
    		    </p>
    		</div>
		<?php endif; ?>
	    </div>

	    <article>
		<?php
		if (isset($page_content)) {
		    echo $page_content;
		};
		if (isset($content)) {
		    echo $content;
		};
		?>
	    </article>
	</section>
    </body>
</html>
