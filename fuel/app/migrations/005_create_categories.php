<?php

namespace Fuel\Migrations;

class Create_categories
{
	public function up()
	{
		\DBUtil::create_table('categories', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'title' => array('constraint' => 20, 'type' => 'title')
		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('categories');
	}
}