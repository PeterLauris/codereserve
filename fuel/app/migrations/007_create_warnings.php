<?php

namespace Fuel\Migrations;

class Create_warnings
{
	public function up()
	{
		\DBUtil::create_table('warnings', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'user_id' => array('constraint' => 11, 'type' => 'int'),
			'type' => array('constraint' => 11, 'type' => 'int'), //1 - warning, 2 - ban
			'reason' => array('type' => 'text'),
			'created_at' => array('type' => 'datetime'),
			'expires_at' => array('type' => 'datetime')
		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('warnings');
	}
}