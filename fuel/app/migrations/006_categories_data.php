<?php

namespace Fuel\Migrations;

class Demo_data
{
	public function up()
	{
	    $categories = ['C', 'C++', 'Java', 'JavaScript', 'Objective C', 'PHP', 'Python', 'Ruby', 'Shell'];
	    foreach ($categories as $category) {
			$cat = \Model_Orm_Category::forge();
			$cat->title = $category;
			$cat->save();
	    }
	}

	public function down()
	{
		\DBUtil::truncate_table('categories');
	}
}