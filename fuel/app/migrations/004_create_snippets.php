<?php

namespace Fuel\Migrations;

class Create_snippets
{
	public function up()
	{
		\DBUtil::create_table('snippets', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'user_id' => array('constraint' => 11, 'type' => 'int'),
			'category_id' => array('constraint' => 11, 'type' => 'int'),
			'title' => array('constraint' => 63, 'type' => 'varchar'),
			'desc' => array('constraint' => 255, 'type' => 'varchar'),
			'content' => array('type' => 'text'),
			'category_id' => array('constraint' => 1, 'type' => 'tinyint'),
			'created_at' => array('type' => 'datetime'),
			'updated_at' => array('type' => 'datetime')

		), array('id'));

		\DBUtil::create_table('ratings', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'user_id' => array('constraint' => 11, 'type' => 'int'),
			'snippet_id' => array('constraint' => 11, 'type' => 'int'),
			'rating' => array('constraint' => 11, 'type' => 'int'),
			'created_at' => array('type' => 'datetime'),
			'updated_at' => array('type' => 'datetime')

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('snippets');
		\DBUtil::drop_table('ratings');
	}
}